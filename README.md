# Classification des Sentiments des Commentaires de Film

## Introduction

Ce projet a pour objectif de classifier les commentaires de film en deux catégories : positifs et négatifs. Pour ce faire, nous utilisons des techniques de prétraitement des données, la vectorisation de texte et la classification supervisée. Les modèles testés sont le Naive-Bayes (BernoulliNB) et le Support Vector Machine (SVM via SVC). Le projet inclut également une évaluation des performances des modèles à travers des rapports de classification et des matrices de confusion.

## Objectifs

1. **Prétraitement des commentaires** : Nettoyer et lemmatiser les commentaires afin de supprimer les éléments non pertinents (ponctuation, stop-words, etc.).
2. **Vectorisation des données** : Transformer les textes en vecteurs numériques en utilisant la technique TF-IDF.
3. **Entraînement de modèles de classification** : Tester et comparer les performances des modèles SVM et Naive-Bayes.
4. **Évaluation des modèles** : Utiliser des métriques standards et des matrices de confusion pour évaluer la performance des modèles.

## Démarche

### 1. Importation des Librairies

- **Pandas** : Pour la manipulation de données.
- **NLTK** : Pour le traitement de la langue naturelle, y compris la tokenisation, le POS tagging et la lemmatisation.
- **Scikit-learn** : Pour la vectorisation TF-IDF, la création de pipelines de modèles, l'entraînement de modèles et l'évaluation des performances.
- **Time** : Pour mesurer le temps d'exécution des modèles.

### 2. Téléchargement des Données

Le corpus utilisé est composé de 2000 commentaires de film fournis par Bo Pang et Lilian Lee, issu du "Sentiment Polarity Dataset Version 2". Il comprend 1000 commentaires positifs et 1000 commentaires négatifs.

- Importation des commentaires négatifs et positifs à partir de fichiers texte dans des répertoires distincts.
- Création des DataFrames pour les commentaires négatifs et positifs avec des labels respectifs.
- Fusion des DataFrames dans une seule DataFrame pour faciliter l'entraînement et le test.

### 3. Prétraitement des Données

- **Tokenisation et POS tagging** : Séparation des commentaires en tokens individuels et attribution de tags de parties du discours.
- **Lemmatisation** : Réduction des mots à leurs formes de base.
- **Nettoyage** : Suppression des signes de ponctuation et des stop-words.
- **Unification** : Conversion des tokens lemmatisés restants en textes nettoyés pour remplacer les lignes originales dans X.

### 4. Division des Données

- Séparation des données en ensembles d'entraînement (70%) et de test (30%).

### 5. Entraînement des Modèles

- Choix interactif du modèle à entraîner : SVC ou BernoulliNB.
- Constitution d'une pipeline de vectorisation TF-IDF et de classification.
- Entraînement supervisé du modèle choisi.

### 6. Prédiction et Évaluation

- Prédiction des labels des données de test.
- Calcul des probabilités de prédiction pour chaque classe.
- Génération de rapports de classification pour évaluer les performances.
- Visualisation des matrices de confusion.

## Résultats

- **Rapport de classification** : Affiche les métriques de précision, rappel et F1-score pour les classes positives et négatives.
- **Matrice de confusion** : Visualisation des vraies positives, vraies négatives, fausses positives et fausses négatives.

## Conclusion

Ce projet montre l'application efficace de techniques de traitement de la langue naturelle et de classification supervisée pour la classification de sentiments. Il offre des insights précieux sur les performances comparatives des modèles SVM et Naive-Bayes pour cette tâche spécifique.

## Contributeurs

- **Diego Rossini** - [GitLab](https://gitlab.com/diego.rossini418)
- **Mathilde Charlet** - [GitLab](https://gitlab.com/mathildecharletb)