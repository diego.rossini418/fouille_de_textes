\documentclass{article}

% Language setting
% Replace `english' with e.g. `spanish' to change the document language
\usepackage[french]{babel}
\usepackage{mathptmx}
\usepackage[T1]{fontenc} 


% Set page size and margins
% Replace `letterpaper' with `a4paper' for UK/EU standard size
\usepackage[a4letterpaper,top=3cm,bottom=3cm,left=2cm,right=2cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}


\title{\Huge{Rapport final de comparaison d'algorithmes de classification}}
\newline
\author{\Large{Diego Rossini (22207717) et Mathilde Charlet (22207942)}}
\newline
\date{7 mai 2023}

\begin{document}
\maketitle
\vspace{90mm}
\begin{abstract}
Ce rapport a été écrit par deux étudiants en M1 du master pluriTAL dans le cadre du cours d'introduction à la fouille de textes (LZST001) donné par Yoann Dupont à l'université Sorbonne-Nouvelle.\newline
L'objectif de cet article est la réalisation d'un test de performance sur deux algorithmes de classification automatique : SVM et Naive Bayes. Le corpus est composé de 2 000 commentaires de films classés selon leur polarité : positive ou négative.

\end{abstract}

\newpage
\hspace{50cm}
\large{\tableofcontents}
% Pour ajouter du content "de force" à la table des matières
% \addcontentsline{toc}{section}{Unnumbered Section}

\newpage
\section{Introduction.}
La fouille de textes appartient au domaine de l'Intelligence Artificielle, et plus généralement à la fouille de données. Elle permet d’extraire automatiquement des informations dans de gros corpus de textes. 4 tâches fondamentales la constitue : Recherche d'information, Classification, Annotation et Extraction d'information.\newline 
Ici, nous ne nous intéresserons qu’à la classification. La classification consiste à associer une "classe" à chaque donnée d'entrée. Il est possible de classifier des données en créant des programmes manuellement (des listes de mots connotés négativement ou positivement, par exemple), ou bien automatiquement. L’automatisation de la tâche de classification peut se faire selon deux approches: par apprentissage supervisé ou non-supervisé. Il s’agit dans cet article de tester des algorithmes de classification par apprentissage automatique supervisé, afin de comprendre leur fonctionnement et comparer leur performance.\newline

Pour ce projet, nous avons choisi de travailler sur la polarité des critiques de films. La classification est donc binaire : positif ou négatif. Après une phase d’apprentissage avec des données étiquetées, l’objectif est que le programme soit en mesure de déterminer la polarité d’un nouveau commentaire de manière fiable.\newline
Notre corpus se compose de 1 000 commentaires positifs et de 1 000 commentaire négatifs. Nous lui faisons subir différents pré-traitements puis nous testons deux algorithmes de classification : SVC et BernouilliNB. Ensuite, nous procédons à des expériences afin de tester les performances des algorithmes. Enfin, nous analysons les résultats obtenus.


\section{Objectifs du projet.}
L'objectif du projet est d'entraîner des classifieurs par apprentissage automatique et comparer les performances de différents algorithmes de classification sur une tâche de détermination de la polarité sur un corpus de 2 000 commentaires de films.

\section{Les données et ressources utilisées.}

\subsection{Origine, format, statut juridique.}
Notre corpus est composé de 2 000 commentaires de films dont 1 000 positifs et 1 000 négatifs. Ces données ont été téléchargé via \href{https://www.kaggle.com/datasets/vipulgandhi/movie-review-dataset}{ce lien kaggle}, il s'agit de la polarity dataset v2.0 (3.0Mb). C'est une collection de commentaires de films issus du site IMDB.com, introduite dans \href{https://aclanthology.org/P04-1035.pdf}{Pang/Lee ACL 2004}.  La licence d’utilisation est renseignée dans le descriptif du projet de Pang \& Lee  : \newline

\begin{displayquote}
“Sentiment Polarity Dataset Version 2.0 Bo Pang and Lillian Lee \newline \href{http://www.cs.cornell.edu/people/pabo/movie-review-data/}{http://www.cs.cornell.edu/people/pabo/movie-review-data/} \newline Distributed with NLTK with permission from the authors.” \newline
\end{displayquote} \newline
Les commentaires datent du début des années 2 000. Le format des documents qui composent le corpus est le format texte. La langue employée est l’anglais.


\subsection{Caractéristiques.}
Le corpus de commentaires de Pang \& Lee est composé de 2 000 documents (chaque commentaire est un document) en format texte. Nous avons choisi d’effectuer une classification en deux classes de ces données, positive et négative. Notre corpus est divisé en commentaires positifs (dans le dossier “pos”) et commentaires négatifs (dans le dossier “neg”). Chacun de ces dossiers contient 1 000 commentaires, la distribution des données par classe est donc uniforme. \newline
Chaque document, c’est-à-dire chaque commentaire, a une taille d’environ 803 mots pour les commentaires positifs et 722 pour les commentaires négatifs. La taille des commentaires varie entre 200 mots minimum jusqu’à environ 1100 mots maximum. 


\subsection{Les pré-traitements.}
Afin de pouvoir passer en input à nos modèles des données assimilables, nous avons effectué plusieurs pré-traitements. Pour cela, nous avons utilisé la librairie NLTK. Les pré-traitements effectués sur notre script Python sont les suivants : \newline
\begin{enumerate}
\item Tokenization du texte : nous avons tokenisé chaque commentaire du corpus;\newline
\item Lemmatisation du texte : nous avons lemmatisé chaque token de chaque commentaire. Pour cela, nous avons dû étiqueter chaque token à l’aide de l’outil “pos\_tag” fourni par NLTK. Ensuite nous avons lemmatisé chaque token en indiquant au lemmatiseur de NLTK l’étiquette POS reliée au token;\newline
\item Effacement des signes de ponctuation : nous avons effacé tous les signes de ponctuation présents dans le texte de chaque commentaire. Pour cela, nous avons utilisé la librairie “string” et nous avons bouclé sur tout le texte afin de repérer la ponctuation et l'effacer (string.ponctuation contient une chaîne de caractères où sont présents la plupart des signes de ponctuation, mais nous avons dû en ajouter quelques-uns qui n’étaient pas présents);\newline
\item Effacement des stopwords : nous avons effacé les stopwords présents dans le texte, c'est-à-dire les mots trop fréquents ou à la fonction seulement grammaticale telles que les prépositions, etc. Nous avons dû spécifier à NLTK que la langue sur laquelle nous travaillons est bien l’anglais.
\end{enumerate}

\section{La méthodologie.}
Pour pouvoir mieux travailler sur ce projet, nous avons créé un dépôt GitLab qui est accessible \href{https://gitlab.com/diego.rossini418/fouille_de_textes}{en cliquant ici} . \newline
Pour ce qui concerne le choix de l’outil de calcul et d'implémentation des algorithmes, nous avons décidé d’écrire un script Python et d’utiliser la librairie scikit-learn. Cette librairie présente plusieurs avantages : \newline
\begin{itemize}
    \item facilité d’importation et d’installation;
    \item facilité d’utilisation et documentation détaillée;
    \item séparation des données en X\_train, X\_test, y\_train, y\_test en une seule ligne de script;
    \item emploi d’une Pipeline qui rend très lisible le traitement effectué sur les données et dans laquelle il est possible de tester la méthode de vectorisation la plus adaptée aux données; 
    \item simplicité dans l’exécution des algorithmes et leur paramétrage;
    \item affichage du rapport des évaluations des performances du modèle ainsi que d’une matrice de confusion claire. 
\end{itemize}
\newline
D’ailleurs, le choix d’écrire un script Python et d’utiliser sklearn présentait l’avantage de tester deux algorithmes différents sans devoir pour autant effectuer deux traitements séparées. Effectivement, lors de l’exécution du script, l’utilisateur peut choisir un des deux algorithmes sélectionnés pour effectuer la tâche de classification.
\newline

Au début du projet, nous étions déjà d’accord pour créer un classifieur qui détermine la polarité “positive - négative” des données que nous aurions soumis au modèle. Toutefois, notre première idée était celle de constituer un corpus de commentaires de critiques de chansons françaises contemporaines mais nous nous sommes rendu compte que la collecte de telles données était trop complexe. C'est pour ça que nous nous sommes tournés vers le cinéma et les critiques de films. Effectivement, la recherche de données dans ce domaine est plus simple et nous sommes parvenus à récolter un corpus exploitable et qui répond aux exigences et objectifs de ce projet. \newline
Les algorithmes d’apprentissage automatique supervisés fonctionnent comme suit: les fonctions f recherchées prennent en entrée une donnée x tabulaire (dont les valeurs sont des booléens ou des nombres), et fournissent en résultat le nom d’une classe y (c’est- à-dire une valeur symbolique à prendre parmi un nombre fini de valeurs possibles). \newline

\subsection{Les étapes.}
\begin{enumerate}
    \item La première étape consistait donc à transformer nos données du format txt en format tabulaire. Pour cela, Mathilde a utilisé la librairie Pandas et a récupéré chaque fichier texte (en utilisant glob) pour ensuite créer une DataFrame comprenant deux colonnes : la première qui contient tous les textes des commentaires (nos X) et la deuxième contenant les “labels” (nos y). Il a ensuite fallu mélanger aléatoirement les données afin de ne pas introduire un biais dans la suite de la démarche en traitant des données déjà trop ordonnées. Voici un aperçu de la DataFrame obtenue : \newline
        \begin{figure}[h]
        \centering
        \includegraphics[width=0.4\textwidth]{./1.jpeg}
        \caption{Aperçu de la DataFrame obtenue à l'étape 1.}
        \end{figure}
    
    \item Ensuite Diego s’est occupé de la phase de pré-traitement des données dont nous avons déjà parlé dans la section 3.3. Dans cette section nous montrerons seulement le résultat final d’un des commentaires nettoyé de sa ponctuation et des ses stopwords et lemmatisé après tokenisation : \newline
        \begin{figure}[h]
        \centering
        \includegraphics[width=0.7\textwidth]{./2.jpeg}
        \caption{Aperçu d'un commentaire après la phase de pré-traitement.}
        \end{figure}
    
    Certains tokens nous ont donné plus de difficultés que d'autres; comme le token anglais “‘s” qui peut être autant le verbe “be” que la marque du génitif. Pour cette raison, nous n’avons pas pu le remplacer par le lemme “be”, alors que nous avons pu le faire pour le token “‘re”, qui est toujours l'abbréviation d'une flexion du verbe "be". De plus, certains signes de ponctuation n’ont pas été effacés lorsqu'ils étaient attachés au token suivant (exemple: writer/director). \newline
    
    \item La troisième étape a été le choix des modèles de classification à utiliser. Sklearn permet de construire facilement une Pipeline et d’y insérer le modèle selon le choix de l’utilisateur et la méthode de vectorisation des données souhaitée, ce qui permet de comparer facilement les différents modèles. La Pipeline que nous avons construite vectorise d’abord les données puis initialise un des deux modèles disponibles : SVC (un modèle SVM) et BernoulliNB (Naive-Bayes adapté aux classifications à deux classes). \newline
    Avant même l’initialisation de la Pipeline, Diego a divisé les données en X et en y (train et test) et il a décidé de les séparer sur une base de 70\% des données pour l'entraînement des modèles et 30\% pour l’évaluation. Grâce à sklearn, cette opération a été très simple car sklearn dispose d’une fonction exprès appelée train\_test\_split(). Voici un aperçu de cette étape du travail dans le script Python : \newline
        \begin{figure}[h]
        \centering
        \includegraphics[width=0.9\textwidth]{./4.jpeg}
        \includegraphics[width=0.9\textwidth]{./3.jpeg}
        \caption{Aperçu de la création de la pipeline avec sklearn.}
        \end{figure}
    \item L’avant dernière étape du projet concerne les prédictions du modèle choisi par l’utilisateur sur les données du X\_test. Pour cela, Mathilde a utilisé la fonction “.predict” du classifieur et a sauvegardé les résultats dans une variable “predictions”. La fonction ".predict” du classifieur associe à l’index de chaque commentaire présent dans X\_test l’étiquette “positive” ou “negative” prédite; tandis que la fonction “.predict\_proba” associe à l’index de chaque commentaire les valeurs entre 0 et 1 de la probabilité du commentaire d’appartenir à la classe 1 (negative) ou à la classe 2 (positive). Mathilde a sauvegardé aussi  les valeurs de cette dernière fonction dans une variable “predictions\_proba”. La variable "visu" permet la  visualisation de la classe prédite ou bien des probabilités de prédiction de la classe. Voici un aperçu du script :
        \begin{figure}[h]
        \centering
        \includegraphics[width=0.9\textwidth]{./5.jpeg}
        \caption{Aperçu du script de l'étape 4.}
        \end{figure}

    \newpage
    \item La dernière étape est l’évaluation des modèles. Sklearn permet d’effectuer automatiquement les calculs des principaux scores d’évaluation (accuracy, précision, rappel, F-mesure, macro avg et weighted avg) et aussi la visualisation de la matrice de confusion. Pour la visualisation des résultats et leur analyse voir section 6. 
\end{enumerate}

\subsection{Problèmes rencontrés.}
\begin{enumerate}
    \item Nous avons obtenu la DataFrame contenant tous les 2 000 commentaires du corpus à partir de la concaténation de deux DataFrames, la première contenant les 1 000 commentaires négatifs et la deuxième contenant les 1 000 commentaires positifs. Toutefois, la DataFrame obtenue après concaténation présentait tous les commentaires négatifs dans la première moitié (index 0-999) et les commentaires positifs dans la deuxième (index 1000-1999). Nous nous sommes aperçus qu’une telle disposition des données avait une influence sur les performances du modèle car, lors de la division en X\_train (70\% du corpus) et x\_test (30\% du corpus) les données étaient mal réparties et la plupart des commentaires sur lesquels était entraîné le modèle étaient des commentaires négatifs. La présence du “random\_state=42” dans la fonction sklearn “train\_test\_split” ne semblait pas résoudre le problème. \newline
    Dans le but de mélanger les données de la DataFrame nous avons écrit une ligne de code qui mélange les données et redémarre les indices de 0 et dans le bon ordre (autrement chaque commentaires aurait gardé son indice d’origine). \newline
    \item L’emploi du lemmatiseur nous a créé des difficultés : afin de pouvoir l’employer correctement il est nécessaire d’abord d’étiqueter en POS les tokens de chaque commentaire. C’est seulement après qu’on peut indiquer au lemmatiseur le token à lemmatiser ainsi que son POS (sinon le POS par défaut est NOUN). Une boucle dans le code Python se charge de cette opération pendant la phase de pré-traitement (voir  3.3).
    Voici la partie du code qui effectue le pré-traitement des données :
        \begin{figure}[h]
        \centering
        \includegraphics[width=0.7\textwidth]{./5bis.jpeg}
        \caption{Aperçu du script qui attribue le bon POS aux tokens afin de les lemmatiser.}
        \end{figure}
    \item Deux tests de vectorisation des données ont été effectués : d’abord on a essayé de vectoriser les données avec CountVectorizer, qui vectorise les données sur la base du nombre de fois qu’un token unique apparaît dans chacun des commentaires. Ensuite on a essayé le TFIDF, qui nous a donné de meilleurs résultats au niveau des performances du modèle (SVC a augmenté son accuracy de 5 points et BernoulliNB de 6).

\end{enumerate}




\section{Les expériences réalisées.}

Nous avons choisi de tester deux algorithmes distincts : BernouilliNB et SVC. BernouilliNB est un classifieur probabiliste, tandis que SVC est un séparateur binaire dans un espace vectoriel.

\subsection{Naive Bayes}
Le classifieur Naive Bayes est fondé sur un calcul de probabilités appelées « probabilités Bayesiennes ». Ces algorithmes utilisent le théorème de Bayes, qui énonce une relation fondamentale entre des probabilités
conditionnelles, qu'on peut formuler ainsi:
\[p(A|B) = \frac{p(B|A).p(A)}{p(B)}\]
Ils sont dits ”naifs” parce que les calculs qu’ils réalisent n’ont de sens statistique qu’à condition de faire une hypothèse d’indépendance entre les attributs qui décrivent les données, hypothèse qui est bien sûr abusive (mais que tous les programmes d’apprentissage automatique font de toute façon, à des degrés divers).\newline
Pour ce qui concerne le classifieur BernouilliNB, il s’agit d’un modèle fondé sur les algorithmes Naive Bayes et son utilisation est conseillée sur des données dont un label peut être présent ou pas. Nous supposons que ce modèle s’adapte bien à nos données du moment où il prédit le label de nos données en tant que “négatif” ou “non-négatif”, ce qui revient au même qu’une séparation entre les labels “négatif” et “positif”. \newline
Les paramètres du modèle BernoulliNB sont ceux assignés par défaut par sklearn : \newline
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{./paramNB.jpeg}
    \caption{Aperçu des paramètres de l'algorithme Naive Bayes.}
    \end{figure}
\subsection{SVM}
Le but d’un apprentissage par SVM est de trouver le "meilleur" hyperplan séparateur rendant compte des données d’apprentissage.\newline

Nos données ne sont pas des données linéaires, c’est-à-dire qu'elles ne sont pas représentables sur une ligne droite ou sur un graphe avec une seule dimension. C’est pour cela que nous allons utiliser “l’astuce du noyau”, ou “kernel trick” : en augmentant la dimension de représentation de nos données et en implémentant une fonction qui va séparer correctement nos données nous aurons une division correcte en classes. \newline
Le noyau peut prendre plusieurs types de fonction : linéaire, polynomiale, gaussienne, exponentielle, sigmoidale, ... Nous avons choisi la fonction linéaire car nous voulons séparer linéairement nos données en deux classes. Nous avons aussi tester les autres, et la linéaire est celle qui obtient les meilleurs résultats.
Les réglages du paramètre du modèle SVM sur sklearn est donc le suivant : \newline
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{./paramSVM.jpeg}
    \caption{Aperçu des paramètres de l'algorithme SVC.}
    \end{figure}
    
Le paramètre “probability=True” permet l’affichage des probabilités associées à chaque classe pour chaque commentaire (paramètre: default = False). \newline

\newpage
\section{Les résultats.}
Pour mesurer l’écart entre le résultat du programme et la bonne réponse (la bonne classe), on utilise un outil clé appelé ”matrice de confusion”, qui comptabilise, pour chaque classe, toutes les données bien ou mal rangées. 
Nos critères d’évaluation des classifieurs sont le rappel, la précision, la F-mesure, et le temps de calcul. 

\subsection{Naive Bayes}

Voici le rapport de classification du modèle BernoulliNB :
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./resultNB.jpeg}
    \caption{Aperçu de l'évaluation de BernouilliNB.}
    \end{figure}
    
Voici la matrice de confusion :
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./confusionNB.jpeg}
    \caption{Aperçu de la matrice de confusion de BernouilliNB.}
    \end{figure}

Voici le temps d’exécution de l'entraînement du modèle BernoulliNB :
\newpage
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./timeNB.jpeg}
    \caption{Aperçu du temps d'execution de BernouilliNB.}
    \end{figure}
\newline

Le temps d’exécution du modèle BernoulliNB est très rapide : 0.4 secondes. Nonobstant cette rapidité, les valeurs du rapport d’évaluation restent peu satisfaisantes. BernoulliNB se révèle peu performant concernant les prédictions des commentaires positifs (le rappel est à 0.68, c’est-à-dire que presque 1⁄3 des commentaires positifs qui lui ont été soumis ont été évalués négatifs), et avec une précision inférieure à 80\% (ici, 77\%). Nous avons essayé d’augmenter les performances de ce modèle en augmentant la percentage des données d'entraînement et en diminuant celle des données test (passer de X\_train 70\% - X\_test 30\% à X\_train 80\% - X\_test 20\%) ; toutefois, les résultats ne connaissent pas d’amélioration. \newline

\subsection{SVM}
Voici le rapport de classification du modèle SVC : 
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./resultSVM.jpeg}
    \caption{Aperçu de l'évaluation de SVC.}
    \end{figure}


Voici la matrice de confusion :
\newpage
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./confusionSVM.jpeg}
    \caption{Aperçu de la matrice de confusion de SVC.}
    \end{figure}

Voici le temps d’exécution de l'entraînement du modèle SVC :
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./timeSVM.jpeg}
    \caption{Aperçu du temps d'execution de SVC.}
    \end{figure}
\newline

Le modèle SVM SupportVectorClassification performe nettement mieux par rapport au modèle BernoulliNB (accuracy à 0.85, soit 7 point de plus). La fonction linéaire qui sépare les données ne semble toutefois pas diviser parfaitement les données en deux classes (bien 90 commentaires sur les 600 qui composent le X\_test ont été mal prédits). \newline
Le temps d'entraînement est cependant très long : 22 secondes pour le traitement de 800 commentaires avec 760 mots en moyenne. 



\newpage
\section{Conclusion.}

Pour conclure, nous avons choisi de tester deux algorithmes de classification automatique par apprentissage supervisé sur une tâche de détermination de la polarité : un Naive Bayes (BernouilliNB) et un SVM (SVC). 
\newline
Nous ne sommes pas passés par Weka pour ce projet car nous voulions nous exercer à coder. Nous avons donc écrit un script en Python en utilisant la libraire sklearn, qui présente beaucoup d’avantages. 
\newline\newline

Notre corpus a été fourni par Bo Pang et Lillian Lee, il se compose de 1 000 commentaires de films positifs et 1 000 commentaires négatifs. Il s’appelle Sentiment Polarity Dataset Version 2.0. 
\newline
Nous avons pré-traité ce corpus pour pouvoir l’utiliser dans une pipeline d’apprentissage automatique : nous l’avons passé du format texte au format tabulaire, nous l’avons nettoyé de la ponctuation et des mots trop fréquents et sans contenu sémantique, nous l’avons étiqueté en POS; enfin, nous l’avons lemmatisé. Ensuite nous avons vectorisé les données nettoyées à travers la méthode TfIdf. Puis nous nous sommes penchés sur les paramètres des algorithmes : la fonction du noyau pour le SVC, que nous avons choisi linéaire, s’agissant d’un problème à deux classes.
\newline\newline

Nous avons ensuite évalué les performances de ces deux algorithmes grâce à une matrice de confusion, au rappel, à la précision, à la F-mesure, et au temps d'exécution.
\newline
BernouilliNB marche bien lorsqu’il s’agit d’une classification binaire, il est rapide et assez performant malgré l’approche mathématique beaucoup plus simple qu’un SVM. C’est un modèle probabiliste quasi-incrémental qui marche aussi sur de petit dataset, ou avec de nouvelles données. La performance était ici en dessous de 80\% pour un temps d'exécution de moins de 0.5s, mais d’après les lectures scientifiques faites, il est parfois plus performant qu’un SVM.
\newline\newline
SVC (Support Vector Classifier) est beaucoup plus technique, moins facilement compréhensible et interprétable par les humains. Les erreurs seront donc plus difficilement gérables. De plus, son coût computationnel est élevé, le temps d'exécution est de 40 à 50 fois plus long que BernouilliNB. À chaque nouveau set de données, il faut rallonger le temps d'exécution. Cependant, les résultats sont meilleurs, et sur une tâche de classification telle que la nôtre, avec une petite dataset et un temps d'exécution autour de 20s, les résultats sont à privilégier. 
\newline
SVC est donc meilleur que BernouilliNB dans notre cas.


\newpage
\section{Références et ressources}

\href{https://www.kaggle.com/datasets/vipulgandhi/movie-review-dataset}{https://www.kaggle.com/datasets/vipulgandhi/movie-review-dataset}\newline

\href{https://www.cs.cornell.edu/people/pabo/movie-review-data/}{https://www.cs.cornell.edu/people/pabo/movie-review-data/}\newline

\href{https://aclanthology.org/P04-1035.pdf}{Pang/Lee ACL 2004}

\href{https://www.cs.cornell.edu/home/llee/papers/sentiment.pdf}{Bo Pang, Lillian Lee, and Shivakumar Vaithyanathan, Thumbs up? Sentiment Classification using Machine Learning Techniques, Proceedings of EMNLP 2002.}\newline

\href{https://www.researchgate.net/profile/Fareed-Khaiser/publication/370048956_Sentiment_Analysis_of_Students'_Feedback_on_Institutional_Facilities_Using_Text-Based_Classification_and_Natural_Language_Processing_NLP/links/644fe406809a5350213f0feb/Sentiment-Analysis-of-Students-Feedback-on-Institutional-Facilities-Using-Text-Based-Classification-and-Natural-Language-Processing-NLP.pdf}{SENTIMENT ANALYSIS OF STUDENTS’ FEEDBACK ON INSTITUTIONAL
FACILITIES USING TEXT-BASED CLASSIFICATION AND NATURAL
LANGUAGE PROCESSING (NLP),
Fareed Kaleem Khaiser*, Amna Saad and Cordelia Mason, 2023}\newline

\href{https://stackabuse.com/implementing-svm-and-kernel-svm-with-pythons-scikit-learn/}{https://stackabuse.com/implementing-svm-and-kernel-svm-with-pythons-scikit-learn/}\newline

\href{https://www.engati.com/glossary/kernel-method}{https://www.engati.com/glossary/kernel-method}\newline

\href{https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.BernoulliNB.html}{https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.BernoulliNB.html}\newline

\href{https://thecleverprogrammer.com/2021/07/27/bernoulli-naive-bayes-in-machine-learning/}{https://thecleverprogrammer.com/2021/07/27/bernoulli-naive-bayes-in-machine-learning/}\newline

\href{https://scikit-learn.org/stable/modules/svm.html#svc}{https://scikit-learn.org/stable/modules/svm.html#svc}\newline

\section{Annexes}
\begin{itemize}
    \item Le corpus composé de deux dossiers, "neg" et "pos", chacun contenant 1 000 fichiers txt.
    \item Le script (.py et .ipynb).
    \item Le dossier latex (avec les captures d'écran et ce rapport en pdf).
\end{itemize}
\end{document}